package ma.nextronic.ahmedarif.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;



import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Properties;


import static android.hardware.camera2.CaptureRequest.FLASH_MODE;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION_CAMERA = 1;
    private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_WRITE_EXTERNAL_STORAGE);

        }else{
            Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, MyService.class);

            startService(intent);
        }

    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_CAMERA) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

                Toast.makeText(this, "camera permission granted" + grantResults[0] + grantResults[1], Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, MyService.class);

                startService(intent);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_WRITE_EXTERNAL_STORAGE);
            } else {

                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

            }

        }
        if (requestCode == PERMISSION_WRITE_EXTERNAL_STORAGE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

                Toast.makeText(this, "storage permission granted" + grantResults[0], Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, MyService.class);

                startService(intent);

            } else {

                Toast.makeText(this, "storage permission denied", Toast.LENGTH_LONG).show();

            }

        }
    }//end onRequestPermissionsResult
}
